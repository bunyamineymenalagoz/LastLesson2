using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Demo5 : MonoBehaviour
{

    public GameObject duckBody;
    public GameObject laserMachine;

    private int rotationSpeed = 35;
    private int laserLength = 150;

    public Material redMaterial;
    public Material defaultMaterial;

    Ray ray;
    RaycastHit hit;

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            rotateLaserMachine(0.1f);
        }

        handleRaycast(laserMachine);
    }

    private void rotateLaserMachine(float axis)
    {
        laserMachine.transform.Rotate(0, axis * rotationSpeed, 0, Space.World);
    }

    public void handleRaycast(GameObject handler)
    {
        Debug.Log("Phase_1");
        Debug.DrawRay(handler.transform.position, handler.transform.TransformDirection(Vector3.forward * laserLength), Color.red);

        if (Physics.Raycast(handler.transform.position, handler.transform.TransformDirection(Vector3.forward * laserLength), out hit, laserLength))
        {
            Debug.Log($"Phase_2 {hit.transform.gameObject.name}");

            if (hit.transform.gameObject.name == "Duck")
            {

                Debug.Log("Phase_3");

                // This is time to colliding raycast with duck object...

                duckBody.GetComponent<MeshRenderer>().material = redMaterial;
            }
        }
        else
        {
            duckBody.GetComponent<MeshRenderer>().material = defaultMaterial;

        }
    }

}
