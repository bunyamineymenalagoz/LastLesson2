using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Demo4_3 : MonoBehaviour
{

    public GameObject CannonBall;

    public int fireForce;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ShootBehaviour();
        }
    }

    public void ShootBehaviour()
    {
        Debug.Log("ShootBehaviour");

        CannonBall.SetActive(true);

        var rgb = CannonBall.GetComponent<Rigidbody>();
        rgb.AddForce(CannonBall.transform.forward * fireForce);
    }

}
